# Restore GitLab font

GitLab has decided to use native system font in 2016. This script restores the font that was used before (Source Sans Pro).
