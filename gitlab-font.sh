#!/bin/sh

mkdir -p /opt/gitlab/embedded/service/gitlab-rails/app/assets/fonts
cp fonts/* /opt/gitlab/embedded/service/gitlab-rails/app/assets/fonts/

cp stylesheets/framework/fonts.scss /opt/gitlab/embedded/service/gitlab-rails/app/assets/stylesheets/framework/fonts.scss

sed -i '1s/^/@import "framework\/fonts";\n/' /opt/gitlab/embedded/service/gitlab-rails/app/assets/stylesheets/framework.scss
sed -i 's/^\$regular_font: .\+$/\$regular_font: "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;/' /opt/gitlab/embedded/service/gitlab-rails/app/assets/stylesheets/framework/variables.scss

patch --reverse --strip=1 --directory /opt/gitlab/embedded/service/gitlab-rails < reduce-font-sizes.patch

# Delete a setting that prevents assets compilation
rm /opt/gitlab/etc/gitlab-rails/env/EXECJS_RUNTIME

# The following code is based on GitLab documentation
# https://docs.gitlab.com/omnibus/settings/configuration.html

NO_PRIVILEGE_DROP=true USE_DB=false gitlab-rake assets:precompile
chown -R git:git /var/opt/gitlab/gitlab-rails/tmp/cache

gitlab-ctl restart
